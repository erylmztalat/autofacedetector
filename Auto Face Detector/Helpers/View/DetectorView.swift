//
//  DetectorView.swift
//  Auto Face Detector
//
//  Created by talate on 25.07.2021.
//

import UIKit

class DetectorView: UIView {
            
    lazy var smileImageView : UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "cancel_mark")
        return imageView
    }()
    
    lazy var smileLabel : UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 15)
        label.text = "Smile"
        label.textColor = .white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var leftEyeImageView : UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "cancel_mark")
        return imageView
    }()
    
    lazy var leftEyeLabel : UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 15)
        label.text = "Left Eye Open"
        label.textColor = .white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var rightEyeImageView : UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "cancel_mark")
        return imageView
    }()
    
    lazy var rightEyeLabel : UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 15)
        label.text = "Right Eye Open"
        label.textColor = .white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.clear
        configure()
    }
    
    private func configure() {
        self.addSubview(smileImageView)
        smileImageView.widthAnchor.constraint(equalToConstant: 20).isActive = true
        smileImageView.heightAnchor.constraint(equalToConstant: 20).isActive = true
        smileImageView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        smileImageView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        
        self.addSubview(smileLabel)
        smileLabel.leftAnchor.constraint(equalTo: smileImageView.rightAnchor, constant: 5).isActive = true
        smileLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        smileLabel.topAnchor.constraint(equalTo: smileImageView.topAnchor).isActive = true
        smileLabel.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        
        self.addSubview(leftEyeImageView)
        leftEyeImageView.widthAnchor.constraint(equalToConstant: 20).isActive = true
        leftEyeImageView.heightAnchor.constraint(equalToConstant: 20).isActive = true
        leftEyeImageView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        leftEyeImageView.topAnchor.constraint(equalTo: smileImageView.bottomAnchor, constant: 5).isActive = true
        
        self.addSubview(leftEyeLabel)
        leftEyeLabel.leftAnchor.constraint(equalTo: leftEyeImageView.rightAnchor, constant: 5).isActive = true
        leftEyeLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        leftEyeLabel.topAnchor.constraint(equalTo: leftEyeImageView.topAnchor).isActive = true
        leftEyeLabel.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        
        self.addSubview(rightEyeImageView)
        rightEyeImageView.widthAnchor.constraint(equalToConstant: 20).isActive = true
        rightEyeImageView.heightAnchor.constraint(equalToConstant: 20).isActive = true
        rightEyeImageView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        rightEyeImageView.topAnchor.constraint(equalTo: leftEyeImageView.bottomAnchor, constant: 5).isActive = true
        
        self.addSubview(rightEyeLabel)
        rightEyeLabel.leftAnchor.constraint(equalTo: rightEyeImageView.rightAnchor, constant: 5).isActive = true
        rightEyeLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        rightEyeLabel.topAnchor.constraint(equalTo: rightEyeImageView.topAnchor).isActive = true
        rightEyeLabel.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
    }
    
    func setupDetector(hasSmile:Bool, hasLeftEyeClosed: Bool, hasRightEyeClosed:Bool) {
        smileImageView.image = hasSmile ? UIImage(named: "check") : UIImage(named: "cancel_mark")
        leftEyeImageView.image = !hasLeftEyeClosed ? UIImage(named: "check") : UIImage(named: "cancel_mark")
        rightEyeImageView.image = !hasRightEyeClosed ? UIImage(named: "check") : UIImage(named: "cancel_mark")
    }
    
     required init?(coder aDecoder: NSCoder) {
         fatalError("init(coder:) has not been implemented")
     }
}

