//
//  MainViewController.swift
//  Auto Face Detector
//
//  Created by talate on 25.07.2021.
//

import UIKit

class MainViewController: UIViewController {

    @IBOutlet weak var autoSelfieButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func tappedAutoSelfie(_ sender: Any) {
        let autoFaceVC = AutoFaceViewController()
        autoFaceVC.modalPresentationStyle = .custom
        self.present(autoFaceVC, animated: true)
    }
}
