//
//  AutoFaceViewController.swift
//  Auto Face Detector
//
//  Created by talate on 25.07.2021.
//

import UIKit
import AVFoundation
import Vision

class AutoFaceViewController: UIViewController {
    
    private let captureSession = AVCaptureSession()
    private lazy var previewLayer = AVCaptureVideoPreviewLayer(session: self.captureSession)
    private let videoDataOutput = AVCaptureVideoDataOutput()
    private var faceLayers: [CAShapeLayer] = []
    let shapeLayer = CAShapeLayer()
    var cameraImage: UIImage?
    
    var hasSmile = false
    var hasLeftEyeClosed = false
    var hasRightEyeClosed = false
    
    lazy var closeButton : UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "close_button"), for: .normal)
        button.addTarget(self, action: #selector(tappedCloseButton), for: .touchUpInside)
        return button
    }()
    
    lazy var showButton : UIButton = {
        let button = UIButton()
        button.backgroundColor = .black.withAlphaComponent(0.7)
        button.setTitle("Show Selfie", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.alpha = 0
        button.addTarget(self, action: #selector(tappedShowButton), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    lazy var detectorView : DetectorView = {
        let view = DetectorView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
        configureUI()
    }
    
    func configure() {
        setupCamera()
        captureSession.startRunning()
        
        let center = view.center
        let circulPath = UIBezierPath(arcCenter: center, radius: 150, startAngle: 0, endAngle: 2.0 * CGFloat.pi, clockwise: true)
        
        shapeLayer.path = circulPath.cgPath
        view.layer.addSublayer(shapeLayer)
        shapeLayer.strokeColor = UIColor.red.cgColor
        shapeLayer.fillColor = UIColor.clear.cgColor
    }
    
    func configureUI() {
        closeButton.frame = CGRect(x: 10, y: 40, width: 30, height: 30)
        self.view.addSubview(closeButton)
        
        self.view.addSubview(showButton)
        showButton.widthAnchor.constraint(equalToConstant: 200).isActive = true
        showButton.heightAnchor.constraint(equalToConstant: 45).isActive = true
        showButton.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        showButton.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -80).isActive = true
        
        self.view.addSubview(detectorView)
        detectorView.heightAnchor.constraint(equalToConstant: 70).isActive = true
        detectorView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        detectorView.topAnchor.constraint(equalTo: view.topAnchor, constant: 100).isActive = true
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.previewLayer.frame = self.view.frame
    }
    
    private func setupCamera() {
        let deviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInWideAngleCamera], mediaType: .video, position: .front)
        if let device = deviceDiscoverySession.devices.first {
            if let deviceInput = try? AVCaptureDeviceInput(device: device) {
                if captureSession.canAddInput(deviceInput) {
                    captureSession.addInput(deviceInput)
                    
                    setupPreview()
                }
            }
        }
    }
    
    private func setupPreview() {
        self.previewLayer.videoGravity = .resizeAspectFill
        self.view.layer.addSublayer(self.previewLayer)
        self.previewLayer.frame = self.view.frame
        
        self.videoDataOutput.videoSettings = [(kCVPixelBufferPixelFormatTypeKey as NSString) : NSNumber(value: kCVPixelFormatType_32BGRA)] as [String : Any]

        self.videoDataOutput.setSampleBufferDelegate(self, queue: DispatchQueue(label: "camera queue"))
        self.captureSession.addOutput(self.videoDataOutput)
        
        let videoConnection = self.videoDataOutput.connection(with: .video)
        videoConnection?.videoOrientation = .portrait
    }
    
    func smileDetect(ciImage:CIImage) {
        let cid = CIDetector(ofType: CIDetectorTypeFace, context: nil, options: [CIDetectorAccuracy : CIDetectorAccuracyHigh , CIDetectorSmile : true , CIDetectorTypeFace  : true  ] )

        let faces = cid!.features(in: ciImage, options: [CIDetectorSmile : true , CIDetectorEyeBlink : true , CIDetectorImageOrientation : 1])
        
        for face in faces as! [CIFaceFeature] {
            DispatchQueue.main.async { [weak self] in
                self?.hasSmile = face.hasSmile
                self?.hasLeftEyeClosed = face.leftEyeClosed
                self?.hasRightEyeClosed = face.rightEyeClosed
                self?.detectorView.setupDetector(hasSmile: face.hasSmile, hasLeftEyeClosed: face.leftEyeClosed, hasRightEyeClosed: face.rightEyeClosed)
            }
        }
        
    }
    
    @objc func tappedShowButton() {
        let fullScreenImageView = FullScreenImageView(frame: self.view.bounds)
        fullScreenImageView.setImageView(image: cameraImage)
        self.view.addSubview(fullScreenImageView)
        UIView.animate(withDuration: 0.15) { [weak self] in
            fullScreenImageView.alpha = 1.0
        }
    }
    
    func showSelfieButton() {
        UIView.animate(withDuration: 0.3) { [weak self] in
            self?.showButton.alpha = 1
        }
    }

    func hideSelfieButton() {
        UIView.animate(withDuration: 0.3) { [weak self] in
            self?.showButton.alpha = 0
        }
    }
    
    @objc func tappedCloseButton(){
        self.dismiss(animated: true)
    }
    
}

extension AutoFaceViewController: AVCaptureVideoDataOutputSampleBufferDelegate {
    
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        
        guard let imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else {
          return
        }
        
        let myCIimage = CIImage(cvPixelBuffer: imageBuffer)
        let videoImage = UIImage(ciImage: myCIimage)
        cameraImage = videoImage
        smileDetect(ciImage: myCIimage)

        let faceDetectionRequest = VNDetectFaceLandmarksRequest(completionHandler: { (request: VNRequest, error: Error?) in
            DispatchQueue.main.async {
                self.faceLayers.forEach({ drawing in drawing.removeFromSuperlayer() })

                if let observations = request.results as? [VNFaceObservation] {
                    self.handleFaceDetectionObservations(observations: observations)
                }
            }
        })

        let imageRequestHandler = VNImageRequestHandler(cvPixelBuffer: imageBuffer, orientation: .leftMirrored, options: [:])

        do {
            try imageRequestHandler.perform([faceDetectionRequest])
        } catch {
          print(error.localizedDescription)
        }
    }
    
    private func handleFaceDetectionObservations(observations: [VNFaceObservation]) {
        for observation in observations {
            let faceRectConverted = self.previewLayer.layerRectConverted(fromMetadataOutputRect: observation.boundingBox)
            
            let centerX = faceRectConverted.origin.x + faceRectConverted.width/2
            let centerY = faceRectConverted.origin.y + faceRectConverted.height/2
            
            if (centerX >= view.center.x - 50) && (centerX <= view.center.x + 50) {
                if (centerY >= view.center.y - 50) && (centerY <= view.center.y + 50) {
                    shapeLayer.strokeColor = UIColor.green.cgColor
                    if hasSmile && !hasLeftEyeClosed && !hasRightEyeClosed {
                        showSelfieButton()
                    }else {
                        hideSelfieButton()
                    }
                }else {
                    shapeLayer.strokeColor = UIColor.red.cgColor
                    hideSelfieButton()
                }
            }else {
                shapeLayer.strokeColor = UIColor.red.cgColor
                hideSelfieButton()
            }
        }
    }
}
